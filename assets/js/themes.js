$('.carousel').carousel({
  
});
$(".carousel").swipe({

  swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

    if (direction == 'left') $(this).carousel('next');
    if (direction == 'right') $(this).carousel('prev');

  },

});



var swiper = new Swiper('.swiper-container', {
      scrollbar: {
        el: '.swiper-scrollbar'
      },
      loop:true
    });

$(".arrow .fa").click(function(event) {
	if ($(this).hasClass('fa-arrow-left')) {
			$(this).parent().parent().carousel("prev");
			
	}
	if ($(this).hasClass('fa-arrow-right')) {
			$(this).parent().parent().carousel("next");
	}
});
// ===========Arrow Header=============

var arrow = $(".arrow-main-header");
arrow.find('.arrow-prev').click(function(event) {
	$("#SlideHeader").carousel("prev");
});
arrow.find('.arrow-next').click(function(event) {
	$("#SlideHeader").carousel("next");
});



/*
	======== Offset =========
*/
var slide_header = $("#SlideHeader");
var slide_content = $("#SlideContent");
var offsetHeader = slide_header.offset().top;
var heightHeader = slide_header.height();
var offsetContent = slide_content.offset().top;
var heightContent = slide_content.height();
$(window).scroll(function(event) {
	var top  = $(this).scrollTop();
	if (top >= offsetHeader && top <= (offsetHeader+heightHeader)) {
		
		actionPerform("head");
	}
	if (top >= offsetContent && top <= (offsetContent+heightContent)) {
		// console.log("content");
		actionPerform("content");
	}
});
function actionPerform(offset){
	var slide;
	if (offset == "head") {
	 slide = $("#SlideHeader");
	}
	if (offset == "content") {
		slide = $("#SlideContent");
	}
	if (slide!=null) {
		$(window).on('keydown', function(event) {
			if (event.which == 37) {
				slide.carousel("prev");
			}
			if (event.which == 39) {
				slide.carousel("next");
			}
		});
	}
}
// =============Humberger===========
var spanArr = $(".menu-responsive").find('span');
$(".navigator").append('<div class="momo"></div>');
$(".momo").click(function(event) {
	actionPerform_inactive_menu();
});

$(".menu-responsive").click(function(event) {
	if (spanArr.eq(0).hasClass('active_1')) {
		actionPerform_inactive_menu();
	}else{
		actionPerform_active_menu();	
	}
});
function momoClear(bool,element){
	if (!bool) {
		element.css('display', 'none');
	}else{
		element.css('display', 'block');
	}
}
function actionPerform_menu(check)
{
	if (check) {
		$(".list-menu").addClass('active');
	}else{
		$(".list-menu").removeClass('active');
	}
}
function actionPerform_active_menu(){
		spanArr.eq(0).addClass('active_1');
		spanArr.eq(1).css('opacity', '0');
		spanArr.eq(2).addClass('active_2');
		actionPerform_menu(true);
		momoClear(true,$(".momo"));
}
function actionPerform_inactive_menu(){
		spanArr.eq(0).removeClass('active_1');
		spanArr.eq(1).css('opacity', '1');
		spanArr.eq(2).removeClass('active_2');
		actionPerform_menu(false);
		momoClear(false,$(".momo"));
}

// ============ Get Width ============

var widthScreen = $(window).innerWidth();
console.log(widthScreen);
if (widthScreen <=767 && widthScreen >480) {
	$("h2.slogan").find('br').remove();
	var images = document.getElementsByClassName("images-home-1").outerHTML;
	console.log(images);
}
if (widthScreen<=480) {
	$('h2.slogan').prepend("<span class='brands'>Sunshine Empire<span><br>");
}

